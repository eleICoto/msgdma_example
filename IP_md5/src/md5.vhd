------------------------------------------------------------------------------------------
-- HEIG-VD
-- Haute Ecole d'Ingenerie et de Gestion du Canton de Vaud
-- School of Business and Engineering in Canton de Vaud
------------------------------------------------------------------------------------------
-- REDS Institute
-- Reconfigurable Embedded Digital Systems
------------------------------------------------------------------------------------------
--
-- File                 : md5.vhd
-- Author               : Sébastien Masle
-- Date                 : 22.03.2019
--
-- Context              : SOCF lab
--
------------------------------------------------------------------------------------------
-- Description : hardware md5 algorithm to speed soft processing up
--
------------------------------------------------------------------------------------------
-- Dependencies :
--
------------------------------------------------------------------------------------------
-- Modifications :
-- Ver    Date        Engineer      Comments
-- 0.0    22.03.2019  SMS           Initial version.
-- 0.1    18.04.2019  SMS           add counter to avalon source data instead of connecting
--                                  sink to source
--
------------------------------------------------------------------------------------------
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity md5 is
    generic (DATA_SIZE_g : natural := 32);
    port (  clk_i                    : in  std_logic;
            rst_i                    : in  std_logic;
            avalon_st_sink_data_i    : in  std_logic_vector(DATA_SIZE_g-1 downto 0);
            avalon_st_sink_valid_i   : in  std_logic;
            avalon_st_sink_ready_o   : out std_logic;
            avalon_st_source_data_o  : out std_logic_vector(DATA_SIZE_g-1 downto 0);
            avalon_st_source_valid_o : out std_logic;
            avalon_st_source_ready_i : in  std_logic
           );
end md5;

architecture algorithm of md5 is

    signal avalon_st_source_ready_reg_s : std_logic;
    signal counter_s : unsigned(DATA_SIZE_g-1 downto 0);

begin

    avalon_st_sink_ready_o <= '1';
    
    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
            counter_s <= (others => '0');
        elsif rising_edge(clk_i) then
            if avalon_st_source_ready_i = '1' and avalon_st_source_ready_reg_s = '1' then
                counter_s <= counter_s + 1;
            end if;
        end if;
    end process;

    process(clk_i, rst_i)
    begin
        if rst_i = '1' then
	    avalon_st_source_ready_reg_s <= '0';
        elsif rising_edge(clk_i) then	    
            avalon_st_source_ready_reg_s <= avalon_st_source_ready_i;
	end if;
    end process;
    
    avalon_st_source_data_o <= std_logic_vector(counter_s);
    avalon_st_source_valid_o <= avalon_st_source_ready_reg_s and avalon_st_source_ready_i;

end algorithm;
